PizzaCostCalculator for Android
===============================

(c) 2011-2022 by Markus Hoffmann

<img alt="Logo" src="fastlane/metadata/android/en-US/images/icon.png" width="120" />

Calculates the optimal offer out of three round pizzas.

This little app calculates which pizza is the best offer, based on diameter and
price.   
Use gestures to adjust price and diameter of three pizzas (small,
medium, large) to compare.  The winner will change color (turn green).


PizzaCostCalculator für Android
===============================

(c) 2011-2022 Markus Hoffmann

Berechnet das beste Angebot aus einer Auswahl von drei runden Pizzas.


Mit diesem kleinen Programm können Sie bestimmen, welches Pizza-Angebot das
günstigste ist. Hierzu werden drei Pizzen  (klein, mittel und gross) aufgrund
von Durchmesser und Preis verglichen. Stellen Sie Preis und Durchmesser durch
Antippen und Aufzoomen (durch Gesten) ein.  Das beste Angebot ist grün.

### Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/app/de.drhoffmannsoft.pizza)

### Important Note:

No software can be perfect. We do our best to keep this app bug free, 
improve it and fix all known errors as quick as possible. 
However, this program is distributed in the hope that it will 
be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Use this program on your own risk. 
Please report all errors, so we can fix them. 


    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.

 git clone git@codeberg.org:kollo/PizzaCostCalculator.git

then do a 
  cd PizzaCostCalculator
  ./gradlew build
(Enter passwords for the keystore)
(the compile process will take a while.)

The apk should finally be in build/outputs/apk/PizzaCostCalculator-release.apk
