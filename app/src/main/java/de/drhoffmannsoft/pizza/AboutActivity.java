package de.drhoffmannsoft.pizza;

/* AboutActivity.java (c) 2011-2022 by Markus Hoffmann 
 *
 * This file is part of PizzaCostCalculator for Android 
 * ==================================================================
 * PizzaCostCalculator for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class AboutActivity extends Activity {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.info);
    Button fertig = findViewById(R.id.okbutton);
    RadioButton meter = findViewById(R.id.meter);
    final RadioButton inch=findViewById(R.id.inch);
    TextView readme = findViewById(R.id.description);
    readme.setText(Html.fromHtml(getResources().getString(R.string.readme)+
        	    getResources().getString(R.string.news)+
		    getResources().getString(R.string.impressum)
        	    ));
    fertig.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        if(inch.isChecked()) PizzacostActivity.munit=1;
        else PizzacostActivity.munit=0;
        finish();
      }
    }); 
    meter.setChecked(PizzacostActivity.munit==0);
    inch.setChecked(PizzacostActivity.munit!=0);
  }
}
