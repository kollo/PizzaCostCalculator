package de.drhoffmannsoft.pizza;

/* PreferencesActivity.java (c) 2011-2022 by Markus Hoffmann 
 *
 * This file is part of PizzaCostCalculator for Android 
 * ==================================================================
 * PizzaCostCalculator for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;

public class PreferencesActivity extends PreferenceActivity  implements OnSharedPreferenceChangeListener {
  private final static String HOMEPAGE="https://codeberg.org/kollo/PizzaCostCalculator";
  private final static String LICENSE_PAGE="https://codeberg.org/kollo/PizzaCostCalculator/raw/branch/master/LICENSE";
  private final static String SOURCE_PAGE="https://codeberg.org/kollo/PizzaCostCalculator";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    addPreferencesFromResource(R.xml.preferences);
    findPreference("about_version").setSummary(applicationVersion());
    findPreference("about_homepage").setSummary(HOMEPAGE);
    for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++) {
      initSummary(getPreferenceScreen().getPreference(i));
    }
  }
  
  @Override
  protected void onResume() {
    super.onResume();
    // Set up a listener whenever a key changes
    getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
  }

  @Override
  protected void onPause() {
    super.onPause();
    // Unregister the listener whenever a key changes
    getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
  }
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) { 
    updatePrefSummary(findPreference(key));
    PizzacostActivity.mcurrency=sharedPreferences.getString("select_currency", "€");
    PizzacostActivity.munit=(int)Double.parseDouble(sharedPreferences.getString("select_units", "0"));
  }

  private void initSummary(Preference p) {
    if(p instanceof PreferenceCategory) {
      PreferenceCategory pCat=(PreferenceCategory) p;
      for(int i=0;i<pCat.getPreferenceCount();i++) initSummary(pCat.getPreference(i));
    } else updatePrefSummary(p);
  }

  private void updatePrefSummary(Preference p) {
    if(p instanceof ListPreference) {
      ListPreference listPref = (ListPreference) p; 
      p.setSummary(listPref.getEntry()); 
    }
    if(p instanceof EditTextPreference) {
      EditTextPreference editTextPref = (EditTextPreference) p; 
      p.setSummary(editTextPref.getText()); 
    }
  }

  private final String applicationVersion() {
    try {return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;}
    catch (NameNotFoundException x)  {return "unknown";}
  }

  @Override
  public boolean onPreferenceTreeClick(final PreferenceScreen preferenceScreen, final Preference preference)  {
    final String key = preference.getKey();
    if("about_license".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LICENSE_PAGE)));
      finish();
    } else if("about_homepage".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HOMEPAGE)));
      finish();
    } else if ("about_version".equals(key)) {
      startActivity(new Intent(PreferencesActivity.this, AboutActivity.class));
      finish();
    } else if("about_source".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SOURCE_PAGE)));
      finish();
    } else if ("about_market_app".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", getPackageName()))));
      finish();
    } else if ("about_market_publisher".equals(key))  {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:\"Markus Hoffmann\"")));
      finish();
    }
    return false;
  }
}
