package de.drhoffmannsoft.pizza;

/* PizzacostActivity.java (c) 2011-2022 by Markus Hoffmann 
 *
 * This file is part of PizzaCostCalculator for Android 
 * ==================================================================
 * PizzaCostCalculator for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;

public class PizzacostActivity extends Activity {
	PizzaView pizza1,pizza2,pizza3;
	EditText price1,price2,price3;
	EditText size1,size2,size3;
	public static int munit;	
	public static String mcurrency="€";
	private float d1,d2,d3;
	private float p1,p2,p3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        mcurrency=prefs.getString("select_currency", "€");
        
        SharedPreferences settings =getPreferences(MODE_PRIVATE);
        p1 = settings.getFloat("p1", (float) 5.87);
        p2 = settings.getFloat("p2", (float) 8.80);
        p3 = settings.getFloat("p3", (float) 10.90);
        d1 = settings.getFloat("d1", (float) 19);
        d2 = settings.getFloat("d2", (float) 26);
        d3 = settings.getFloat("d3", (float) 30);
        munit = settings.getInt("munit", 0);

        pizza1= this.findViewById(R.id.pizza1);
        pizza2= this.findViewById(R.id.pizza2);
        pizza3= this.findViewById(R.id.pizza3);
        price1= this.findViewById(R.id.edit1p);
        price2= this.findViewById(R.id.edit2p);
        price3= this.findViewById(R.id.edit3p);
        size1=  this.findViewById(R.id.edit1d);
        size2=  this.findViewById(R.id.edit2d);
        size3=  this.findViewById(R.id.edit3d);

        price1.setText(""+Math.round(p1*100)/100.0);
        price1.addTextChangedListener(new TextWatcher(){
        	public void onTextChanged(CharSequence s, int start, int before, int count) {}
           	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        	public void afterTextChanged(Editable s) {
        		if(s.length()>0) {
        			p1=(float) Double.parseDouble(s.toString()); 
        		    pizza1.setp(p1);
        		}
        		redraw();
        	}
        });

        price2.setText(""+Math.round(p2*100)/100.0);
        price2.addTextChangedListener(new TextWatcher(){
        	public void onTextChanged(CharSequence s, int start, int before, int count) {}
           	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        	public void afterTextChanged(Editable s) {
        		if(s.length()>0) {
        			p2=(float) Double.parseDouble(s.toString());
        		    pizza2.setp(p2);
        		}
        		redraw();
        	}
        });

        price3.setText(""+Math.round(p3*100)/100.0);
        price3.addTextChangedListener(new TextWatcher(){
        	public void onTextChanged(CharSequence s, int start, int before, int count) {}
           	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        	public void afterTextChanged(Editable s) {
        		if(s.length()>0) {
        			p3=(float) Double.parseDouble(s.toString());
        		    pizza3.setp(p3);
        		}
        		redraw();
        	}
        });
	    size1.setText(""+Math.round(d1*10)/10.0);
	    size1.addTextChangedListener(new TextWatcher(){
	        	public void onTextChanged(CharSequence s, int start, int before, int count) {}
	           	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	        	public void afterTextChanged(Editable s) {
	        		if(s.length()>0) {
	        			d1=(float) Double.parseDouble(s.toString());
	        		    pizza1.setd(d1);
	        		}
	        		redraw();
	        	}
	        });
		size2.setText(""+Math.round(d2*10)/10.0);
	    size2.addTextChangedListener(new TextWatcher(){
        	public void onTextChanged(CharSequence s, int start, int before, int count) {}
           	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        	public void afterTextChanged(Editable s) {
        		if(s.length()>0) {
        			d2=(float) Double.parseDouble(s.toString());
        		    pizza2.setd(d2);
        		}
        		redraw();
        	}
        });
		size3.setText(""+Math.round(d3*10)/10.0);
	    size3.addTextChangedListener(new TextWatcher(){
        	public void onTextChanged(CharSequence s, int start, int before, int count) {}
           	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        	public void afterTextChanged(Editable s) {
        		if(s.length()>0) {
        			d3=(float) Double.parseDouble(s.toString());
        		    pizza3.setd(d3);
        		}
        		redraw();
        	}
        });
        pizza1.setd(d1);
        pizza2.setd(d2);
        pizza3.setd(d3);
        pizza1.setp(p1);
        pizza2.setp(p2);
        pizza3.setp(p3);
        pizza1.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				boolean ret=pizza1.onTouch(v,event);
				redraw();
				return ret;
			}
        });
        pizza2.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				boolean ret=pizza2.onTouch(v,event);
				redraw();
				return ret;
			}
        });
        pizza3.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				boolean ret=pizza3.onTouch(v,event);
				redraw();
				return ret;
			}
        });
        redraw();
    }
    @Override
    protected void onStop(){
        super.onStop();

       // We need an Editor object to make preference changes.
       // All objects are from android.context.Context
       SharedPreferences settings = getPreferences(MODE_PRIVATE);
       SharedPreferences.Editor editor = settings.edit();
       editor.putFloat("d1", d1);
       editor.putFloat("d2", d2);
       editor.putFloat("d3", d3);
       editor.putFloat("p1", p1);
       editor.putFloat("p2", p2);
       editor.putFloat("p3", p3);
       editor.putInt("munit", munit);
       // Commit the edits!
       editor.commit();
     }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Intent viewIntent;
        switch (item.getItemId()) {
            case R.id.infotext:    
            	viewIntent = new Intent(PizzacostActivity.this, AboutActivity.class);
            	startActivity(viewIntent);
            	break;
            case R.id.settings:     
            	viewIntent = new Intent(PizzacostActivity.this, PreferencesActivity.class);
            	startActivity(viewIntent);
            	break;
            case R.id.finish: 
            	finish();
                break;
            default: 
            	return super.onOptionsItemSelected(item);
        }
        return true;
    }
    private void redraw() {
	if(p1!=pizza1.pizzap) {
    		p1=pizza1.pizzap;
    		price1.setText(""+Math.round(p1*100)/100.0);
    	}
    	if(p2!=pizza2.pizzap) {
    		p2=pizza2.pizzap;
    		price2.setText(""+Math.round(p2*100)/100.0);
    	}
    	if(p3!=pizza3.pizzap) {
    		p3=pizza3.pizzap;
    		price3.setText(""+Math.round(p3*100)/100.0);
    	}
    	if(d1!=pizza1.pizzad) {
    		d1=pizza1.pizzad;
    		size1.setText(""+Math.round(d1*10)/10.0);
    	}
    	if(d2!=pizza2.pizzad) {
    		d2=pizza2.pizzad;
    		size2.setText(""+Math.round(d2*10)/10.0);
    	}
    	if(d3!=pizza3.pizzad) {
    		d3=pizza3.pizzad;
    		size3.setText(""+Math.round(d3*10)/10.0);
    	}
    	double c1=p1/Math.PI/d1/d1;
    	double c2=p2/Math.PI/d2/d2;
    	double c3=p3/Math.PI/d3/d3;

    	pizza1.setwinner(c1<=c2 && c1<=c3);
       	pizza2.setwinner(c2<=c1 && c2<=c3);
        pizza3.setwinner(c3<=c1 && c3<=c2);
        pizza1.invalidate();
        pizza2.invalidate();
        pizza3.invalidate();   
    }
}
