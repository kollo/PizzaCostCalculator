package de.drhoffmannsoft.pizza;

/* PizzaView.java (c) 2011-2022 by Markus Hoffmann 
 *
 * This file is part of PizzaCostCalculator for Android 
 * ==================================================================
 * PizzaCostCalculator for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class PizzaView  extends View {
	private Paint paint;
	private int bx,by,bw,bh;
	private int sx,sy,sw,sh;
	public float pizzad,pizzap,size;
	private boolean winner=false;
	private GestureDetector mgd;
	private ScaleGestureDetector mscalegd;

	public void setwinner(boolean f) {
		winner=f;
	}
	
	public void setd(float d) {
       		pizzad=d;
	}
	public void setp(float p) {
		pizzap=p;
	}
	public PizzaView(Context context) {
        	this(context, null, 0);
    	}
    	public PizzaView(Context context, AttributeSet attrs) {
     	  	this(context, attrs, 0);
      	}
    	public PizzaView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		setFocusable(true); //necessary for getting the touch events
		setFocusableInTouchMode(true);
		// this.setOnTouchListener(this);
		mscalegd = new ScaleGestureDetector(context,new MySimpleOnScaleGestureListener());
		paint=new Paint();
		mgd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
        		public boolean onDoubleTap(MotionEvent e) {
    		  		invalidate();
    				return false;
    			}
        		public void onLongPress(MotionEvent e) {
    		  		invalidate();
    			}
        		public void onMove(float dx, float dy) {
        			dx=(float) (dx/(float)sw*2.0);
    	        		pizzap+=dx;
    	        		if(pizzap<1) pizzap=1;
        	    		invalidate();
        		}
        		public void onResetLocation() {
        	    		invalidate();
        		}
        		@Override
        		public boolean onScroll(MotionEvent e1, MotionEvent e2,float distanceX, float distanceY) {
 				onMove(-distanceX, -distanceY);
				return true;
        		}
			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				final float distanceTimeFactor = 0.1f;
				final float totalDx = (distanceTimeFactor * velocityX/2);
				final float totalDy = (distanceTimeFactor * velocityY/2);
				// this.onAnimateMove(totalDx, totalDy,(long) (1000 * distanceTimeFactor));
				return false;
            		}
        	});
      		//  paint.setStrokeWidth(1);
      		//  paint.setStyle(Paint.Style.STROKE);
    		paint.setAntiAlias(false);
        	paint.setStyle(Style.FILL_AND_STROKE);
        	paint.setColor(Color.BLUE);
        	paint.setTextSize(20);
      	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int modex=MeasureSpec.getMode(widthMeasureSpec);
		int sizex=MeasureSpec.getSize(widthMeasureSpec);
		int modey=MeasureSpec.getMode(heightMeasureSpec);
		int sizey=MeasureSpec.getSize(heightMeasureSpec);
		if (modex == MeasureSpec.UNSPECIFIED) sw=200;
		else sw=sizex;
		if (modey == MeasureSpec.UNSPECIFIED) sh=280;
		else sh=sizey;
		setMeasuredDimension(sw, sh);
	}
	@Override
	public void onDraw(Canvas canvas) {
		sw=getMeasuredWidth();
		sh=getMeasuredHeight();
		bw=Math.min(sw,sh);
		float tf=(float) (bw/150.0);

		canvas.drawColor(Color.BLACK);
		paint.setTextSize(22*tf);
		paint.setStyle(Paint.Style.STROKE);
		paint.setAntiAlias(true);
		paint.setStrokeWidth(2);
        
		/*Draw the pizza in the center*/
		if(winner) paint.setColor(Color.GREEN);
		else paint.setColor(Color.WHITE);
          
		canvas.drawCircle(sw/2,sh/2,(int)((double)bw/2*pizzad/30.0), paint);
        
		canvas.drawLine(sw/2-(int)((double)bw/2*pizzad/30.0),sh/2+bw/2+30,sw/2+(int)((double)bw/2*pizzad/30.0),sh/2+bw/2+30, paint);
		canvas.drawLine(sw/2-(int)((double)bw/2*pizzad/30.0),sh/2+bw/2+20,sw/2-(int)((double)bw/2*pizzad/30.0),sh/2+bw/2+40, paint);
		canvas.drawLine(sw/2+(int)((double)bw/2*pizzad/30.0),sh/2+bw/2+20,sw/2+(int)((double)bw/2*pizzad/30.0),sh/2+bw/2+40, paint);
       
		int i;
		for(i=-3;i<4;i++) {
			canvas.drawLine(sw/2+(int)((double)bw/2*pizzad/30.0),sh/2+bw/2+30,sw/2+(int)((double)bw/2*pizzad/30.0)-20,sh/2+bw/2+i+30, paint);	
			canvas.drawLine(sw/2-(int)((double)bw/2*pizzad/30.0),sh/2+bw/2+30,sw/2-(int)((double)bw/2*pizzad/30.0)+20,sh/2+bw/2+i+30, paint);	
		}

		paint.setStrokeWidth(0);
        
		String s=""+Math.round(100.0*10*pizzap/pizzad*2/pizzad*2/Math.PI)/10.0+" ct/cm²";
		canvas.drawText(s,sw/2-paint.measureText(s)/2,sh/2+5,paint);
		/*Draw scaler 1*/
		paint.setTextSize(32*tf);
		s=""+Math.round(pizzap*100.0)/100.0+" "+PizzacostActivity.mcurrency;
		canvas.drawText(s,sw/2-paint.measureText(s)/2,5+paint.measureText("W"),paint);
		if(PizzacostActivity.munit==0) s=""+Math.round(pizzad*10)/10.0+" cm";
		else s=""+Math.round(cm2inch(pizzad)*10)/10.0+" \"";
		canvas.drawText(s,sw/2-paint.measureText(s)/2,sh-30,paint);
        
		/*Draw scaler 2*/
        
      //  paint.setAntiAlias(false);
//        canvas.drawLine(bx+bw/2-6,kt(tmin),bx+bw/2-6,kt(tmax), paint);
//        canvas.drawLine(bx+bw/2+6,kt(tmin),bx+bw/2+6,kt(tmax), paint);
               
//        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        
//        paint.setColor(Color.BLACK);
//        canvas.drawRect(bx+bw/2-5,by,bx+bw/2+6,by+bh, paint);
        
//        canvas.drawCircle(bx+bw/2,sy+sh-14,13, paint);
//        canvas.drawRect(bx+bw/2-5,kt(temperature),bx+bw/2+6,by+bh, paint);
//  	  paint.setColor(Color.WHITE);

//  	  canvas.drawLine(bx+bw/2-6,kt(tempmin),bx+bw/2+7,kt(tempmin), paint);
//  	  canvas.drawLine(bx+bw/2-6,kt(tempmax),bx+bw/2+7,kt(tempmax), paint);
//  	  if(Math.signum(tmin)!=Math.signum(tmax)) canvas.drawLine(bx+bw/2-10,kt(0),bx+bw/2+12,kt(0), paint);

 // 	  canvas.drawLine(bx+bw/2+20,by,bx+bw/2+20,by+bh, paint);
//  	paint.setAntiAlias(true);
//  	String s;
//  	if(TaupunktActivity.munit==0) s=""+Math.round(temperature*10)/10.0+"°C";
//  	else s=""+Math.round(c2f(temperature)*10)/10.0+"°F";
//  	canvas.drawText(s,bx+bw/2-8-paint.measureText(s),by+bh/2,paint);
  	
	}
	public boolean onTouch(View view, MotionEvent event) {
		mscalegd.onTouchEvent(event);
		mgd.onTouchEvent(event);
		return true;
    /*	if(event.getY()>sh/2) {
    		if(event.getX()>sw/2) {
        		pizzad+=1;
        		if(pizzad>32) pizzad=32;
        	} else {
        		pizzad-=1;
        		if(pizzad<9) pizzad=9;
        	}
    	} else {
    		if(event.getX()>sh/2) {
        		pizzap+=0.01;
        	} else {
        		pizzap-=0.01;
        		if(pizzap<1) pizzap=1;
        	}
			pizzap=(float) (Math.floor(pizzap*1000)/1000);
    	}
    	
      invalidate();
      return true; */
	}
		public class MySimpleOnScaleGestureListener extends SimpleOnScaleGestureListener {
			@Override
			public boolean onScale(ScaleGestureDetector detector) {
			float scaleFactor = (float) Math.pow(detector.getScaleFactor(),0.1);
			pizzad=pizzad*scaleFactor;
			if(pizzad<9.0) pizzad=(float) 9.0;
			if(pizzad>44.0) pizzad=(float) 44.0;
			invalidate();
			return true;
		}
	}
	double cm2inch(double a) {
		return a/2.54;
	}
}
